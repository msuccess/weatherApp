import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/observable/forkJoin';


@Injectable()
export class NewsProvider {
  endpoint = "https://newsapi.org/v2/top-headlines?sources=";
  apiKey = "&apiKey=dc1caaeb2cb24ae5ac935a28f671abae";

  urlbbc = "bbc-sport";
  urlTalksport = "talksport";
  urlSportbible = "the-sport-bible";
  urlFourfourtwo = "four-four-two";

  constructor(public http: HttpClient) {
  }


  getNewsbbc() {
    return this.http.get(this.endpoint + this.urlbbc + this.apiKey);
  }

  getTalksport() {
    return this.http.get(this.endpoint + this.urlTalksport + this.apiKey);
  }

  getSportbible() {
    return this.http.get(this.endpoint + this.urlSportbible + this.apiKey);
  }

  getFourfourtwo() {
    return this.http.get(this.endpoint + this.urlFourfourtwo + this.apiKey);
  }


}
