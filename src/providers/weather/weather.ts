import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class WeatherProvider {

  apiKey = "63bb1735ddef431e9fc200624181107";
  url

  //key=key&q=coutry
  constructor(public http: HttpClient) {
    this.url = `http://api.apixu.com/v1/current.json?key=${this.apiKey}&q=`;
  }

  getWeather(city){
    return this.http.get(this.url+city);
  }
}
