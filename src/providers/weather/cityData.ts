import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CityDataProvider {

  url="";

  //key=key&q=coutry
  constructor(public http: HttpClient) {
  }

  getCityData(){
    return this.http.get(this.url);
  }

  createCityData(city){
    return this.http.post(this.url,city);
  }
}
