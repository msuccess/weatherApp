import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BbcsportsPage } from './bbcsports';

@NgModule({
  declarations: [
    BbcsportsPage,
  ],
  imports: [
    IonicPageModule.forChild(BbcsportsPage),
  ],
})
export class BbcsportsPageModule { }
