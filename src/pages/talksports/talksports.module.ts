import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TalksportsPage } from './talksports';

@NgModule({
  declarations: [
    TalksportsPage,
  ],
  imports: [
    IonicPageModule.forChild(TalksportsPage),
  ],
})
export class TalksportsPageModule {}
