import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FourfourtwoPage } from './fourfourtwo';

@NgModule({
  declarations: [
    FourfourtwoPage,
  ],
  imports: [
    IonicPageModule.forChild(FourfourtwoPage),
  ],
})
export class FourfourtwoPageModule {}
