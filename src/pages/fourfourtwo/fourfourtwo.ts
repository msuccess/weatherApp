import { NewsProvider } from './../../providers/news/news';
import { Component, OnInit } from '@angular/core';
import { IonicPage, ToastController, LoadingController } from 'ionic-angular';

/**
 * Generated class for the FourfourtwoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fourfourtwo',
  templateUrl: 'fourfourtwo.html',
})
export class FourfourtwoPage implements OnInit {

  fourFourtwo: any;

  constructor(
    private newsProvider: NewsProvider,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    // private browserTab: BrowserTab
  ) {
  }

  ngOnInit(): void {
    this.getFourfourtwo();
  }

  openBrowser(url) {
    console.log();
    // this.browserTab.isAvailable()
    //   .then((isAvailable: boolean) => {

    //     if (isAvailable) {

    //       this.browserTab.openUrl(url);

    //     } else {

    //       // if custom tabs are not available you may  use InAppBrowser
    //     }

    //   });
  }


  getFourfourtwo() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
    })
    loader.present();

    this.newsProvider.getFourfourtwo().subscribe((data: any) => {
      this.fourFourtwo = data.articles
      loader.dismiss();
    }),
      (error: Error) => {
        loader.dismiss();
        console.log(error);

        const toast = this.toastCtrl.create({
          message: "Error Getting News",
          duration: 3000
        });

        toast.present();
      }, () => {
        loader.dismiss();
      }
  }

}
