import { NewsProvider } from './../../providers/news/news';
import { Component, OnInit } from '@angular/core';
import { IonicPage, ToastController, LoadingController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-sportbible',
  templateUrl: 'sportbible.html',
})
export class SportbiblePage implements OnInit {

  sportbible: any;

  constructor(
    private newsProvider: NewsProvider,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    // private browserTab: BrowserTab
  ) {
  }

  ngOnInit(): void {
    this.getSportbible();
  }

  openBrowser(url) {
    console.log();
    // this.browserTab.isAvailable()
    //   .then((isAvailable: boolean) => {

    //     if (isAvailable) {

    //       this.browserTab.openUrl(url);

    //     } else {

    //       // if custom tabs are not available you may  use InAppBrowser
    //     }

    //   });
  }


  getSportbible() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
    })
    loader.present();

    this.newsProvider.getSportbible().subscribe((data: any) => {
      this.sportbible = data.articles
      loader.dismiss();
    }),
      (error: Error) => {
        loader.dismiss();
        console.log(error);

        const toast = this.toastCtrl.create({
          message: "Error Getting News",
          duration: 3000
        });

        toast.present();
      }, () => {
        loader.dismiss();
      }
  }

}
