import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SportbiblePage } from './sportbible';

@NgModule({
  declarations: [
    SportbiblePage,
  ],
  imports: [
    IonicPageModule.forChild(SportbiblePage),
  ],
})
export class SportbiblePageModule {}
