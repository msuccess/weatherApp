import { Component } from '@angular/core';
import { IonicPage, PopoverController, NavController } from 'ionic-angular';
@IonicPage()

@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {

  constructor(
    private popoverCtrl: PopoverController,
    private navCtrl: NavController,
  ) {
  }
  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create('PopoverPage');
    popover.present({
      ev: myEvent
    });
  }

  getBBCNews() {
    this.navCtrl.push('BbcsportsPage');
  }
  getFourfourtwo() {
    this.navCtrl.push('FourfourtwoPage');
  }

  getSportbible() {
    this.navCtrl.push('SportbiblePage');
  }

  getTalksport() {
    this.navCtrl.push('TalksportsPage');
  }

}
