import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { PopoverPageModule } from '../popover/popover.module';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    PopoverPageModule,
    IonicPageModule.forChild(HomePage),
  ],
})
export class HomePageModule { }
