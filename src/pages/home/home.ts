import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, LoadingController, ToastController } from 'ionic-angular';
import { WeatherProvider } from './../../providers/weather/weather';
import { Storage } from "@ionic/storage";
import { WeatherModel } from '../../app/model/weatherModel';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit {

  currentWeather: WeatherModel;
  loading = false;
  location: {
    city: string,
    country: string
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private weatherProvider: WeatherProvider,
    private storage: Storage,
    private popoverCtrl: PopoverController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) {


  }

  getWeatherData() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
    })
    loader.present();
    this.weatherProvider.getWeather(this.location.city).subscribe((data: any) => {
      this.currentWeather = data;
      console.log(this.currentWeather);
    }, (error: Error) => {
      console.log(error);
      loader.dismiss();
      this.loading = false;
      const toast = this.toastCtrl.create({
        message: "Error Getting Weather",
        duration: 3000
      });
      toast.present();
    },
      () => {
        loader.dismiss();
        this.loading = false;
      }
    )
  }


  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create('PopoverPage');
    popover.present({
      ev: myEvent
    });
  }


  ngOnInit() {

    this.storage.get('location').then((val) => {
      if (val != null) {
        this.location = JSON.parse(val);
      }
      else {
        this.location = {
          city: 'Accra',
          country: 'Ghana'
        }
      }

      console.log(this.location);
      this.getWeatherData();
    })


  }

}
