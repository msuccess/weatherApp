import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingsPage } from './settings';
import { IonicStorageModule } from '../../../node_modules/@ionic/storage';


@NgModule({
  declarations: [
    SettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(SettingsPage),
    IonicStorageModule.forRoot()
  ],
})
export class SettingsPageModule {}
