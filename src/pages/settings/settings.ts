import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { HomePage } from '../home/home';
import { CityDataProvider } from './../../providers/weather/cityData';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  city: string;
  country: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private datamodel: CityDataProvider
  ) {
    //ToDO: Save to database
    this.city = "Accra";
    this.country = 'Ghana';
  }

  getCity(){
    this.datamodel.getCityData().subscribe((res: any)=>{
      this.city = res;
    })
  }

  createCity(city){
    this.datamodel.createCityData(city).subscribe(()=>{})
  }


  onSubmit() {
    this.createCity(this.city);
    let location = {
      city: this.city,
      country: this.country
    }
    this.storage.set('location', JSON.stringify(location));
    this.navCtrl.push(HomePage);
  }

}
