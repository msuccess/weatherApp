import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})
export class PopoverPage {

  constructor(
              public navCtrl: NavController,
              public navParams: NavParams,
  ) {
  }

  aboutClk(){
    this.navCtrl.push('AboutPage');
  }
  settingsClk(){
    this.navCtrl.push('SettingsPage');
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverPage');
  }

}
