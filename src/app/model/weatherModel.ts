
export class WeatherModel {
    country: string;
    region: string;
    city: string;
    icon: string;
    text: string;
    temp_f: string;
    humidity: string;
    wind: string;
    wind_dir: string;
    pressure: string;
    cloud: string;


}