import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { IonicStorageModule } from '../../node_modules/@ionic/storage';


import { MyApp } from './app.component';
import { NewsProvider } from '../providers/news/news';
import { WeatherProvider } from '../providers/weather/weather';
import { CityDataProvider } from '../providers/weather/cityData';
import { BrowserTab } from '@ionic-native/browser-tab';



@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    NewsProvider,
    WeatherProvider,
    CityDataProvider,
    BrowserTab
  ]
})
export class AppModule { }
